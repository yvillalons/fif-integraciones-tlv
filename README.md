Fif Integraciones TLV

Diseñada para procesar datos TLV.

Pre-requisitos 📋
GO

Instalación 🔧

Descargar de los sitios oficiales

https://golang.org/

Ejecutando las pruebas ⚙️

go run main.go "0001000d48656c6c6f2c20776f726c642e00020002002a"

Construido con 🛠️

Visual Studio Code

Inicializar API

Autores ✒️
Yethro Villalon (yvillalonsilva@gmail.com)
