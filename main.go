package main

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io"
	"strconv"
	"os"
)

const (
	TagString = 1
	TagUint16 = 2
)

type TLV struct {
	Tag    uint16
	Length uint16
	Value  []byte
}

func ReadTLV(r io.Reader) *TLV {
	var record TLV

	err := binary.Read(r, binary.BigEndian, &record.Tag)
	if err == io.EOF {
		return nil
	} else if err != nil {
		fmt.Println("\n[!] error reading TLV:", err.Error())
		return nil
	}

	err = binary.Read(r, binary.BigEndian, &record.Length)
	if err != nil {
		fmt.Println("\n[!] error reading TLV:", err.Error())
		return nil
	}

	fmt.Println("TAG: ", record.Tag);
	fmt.Println("LARGO: ", record.Length);

	record.Value = make([]byte, record.Length)
	_, err = io.ReadFull(r, record.Value)
	if err != nil {
		fmt.Println("[!] error reading TLV:", err.Error())
		return nil
	}

	fmt.Println("[+] read TLV record")
	return &record
}

func displayTagString(tlv *TLV) (v string){
	fmt.Println("[+] String record:", string(tlv.Value))
	v = string(tlv.Value)
	return v;
}

func displayTagUint16(tlv *TLV) (v string) {
	buf := bytes.NewBuffer(tlv.Value)
	var value uint16

	err := binary.Read(buf, binary.BigEndian, &value)
	if err != nil {
		fmt.Println("[!] Invalid record:", err.Error())
	} else {
		fmt.Println("[+] Uint16 record:", value)
	}

	v = strconv.Itoa(int(value))

	return v
}

func displayTLV(tlv *TLV) (v string) {
	switch tlv.Tag {
	case TagString:
		v = displayTagString(tlv)
	case TagUint16:
		v = displayTagUint16(tlv)
	default:
		v = "";
		fmt.Println("[!] unknown tag", tlv.Tag)
	}

	return v; 
}

func retorno(b []byte) (map[int]string){

	message := bytes.NewBuffer(b)
	var r string
	var record map[int]string
	record = make(map[int]string)

	i := 0
	for {
		
		tlv := ReadTLV(message)
		if tlv == nil {
			break
		}
		r = displayTLV(tlv)
		record[i] = r
		i++
	}

	return record;
}

func main() {

	if len(os.Args) == 1 {
		fmt.Println("[!] Parametro de entrada vacio, vuelva a intentarlo");
		return
	}

	var example = os.Args[1]
	rawMessage, err := hex.DecodeString(example)
	if err != nil {
		fmt.Println("[!] couldn't decode message:", err.Error());
		return
	}

	var d  map[int]string
	
	d = retorno(rawMessage);

	fmt.Println("map:", d)
}
